import {IExample} from "../utils/IExample";
import {Meat, MeatDetails} from "./base/Meat";
import {MeatDatabase, TemperatureType} from "./base/AncientAPI";

export class AdaptorExample implements IExample {
  main(): void {

    // non adapted
    const nonAdaptedBeef = new Meat("Beef");
    nonAdaptedBeef.loadData();

    // without wrapper classes
    const db = new MeatDatabase()
    const name = "Beef";
    console.log(
        name,
        db.getSafeCookTemp(name, TemperatureType.Fahrenheit),
        db.getSafeCookTemp(name, TemperatureType.Celsius),
        db.getCaloriesPerOunce(name),
        db.getProteinPerOunce(name),
    );


    // adapted
    const beef = new MeatDetails("Beef")
    beef.loadData();
  }

}
