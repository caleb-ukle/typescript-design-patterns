import {SoftDrink} from "./SoftDrink";
import {LemonLime} from "./Leaves";

export class Colas {
    availableFlavors: SoftDrink[] = [];
    constructor() {
    }
}

export class RootBeers {
    availableFlavors: SoftDrink[] = [];
    constructor() {
    }
}

export class SodaDispenser {
    colas: Colas;
    lemonLime: LemonLime;
    rootBeers: RootBeers;

    constructor() {
        this.colas = new Colas();
        this.lemonLime = new LemonLime(190);
        this.rootBeers = new RootBeers();
    }

    displayCalories() {
        const sodas = new Map()
        // TODO add reflection
        this.colas.availableFlavors.forEach(a => sodas.set(a, a.calories))
        sodas.set('lemonLime', this.lemonLime.calories);
        this.rootBeers.availableFlavors.forEach(a => sodas.set(a, a.calories));

        console.log(sodas)
    }

}
