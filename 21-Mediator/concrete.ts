import {IChatable, Mediator} from "./Mediator";

export class Intercom implements Mediator {
    private _north: IChatable;
    private _south: IChatable;
    private speakers = [];

    add(i: IChatable) {
        this.speakers.push(i);
    }

    set North(value: IChatable) {
        this._north = value;
    }

    set South(value: IChatable) {
        this._south = value;
    }

    sendMessage(message: string, colleague: IChatable) {

        const item = this.speakers.find(s => s === colleague);

        if (colleague == this._north) {
            this._south.notify(message);
        } else {
            this._north.notify(message);
        }
    }
}
