import {IVisitor} from "./visitors";

export abstract class Element {
    abstract accept(visitor: IVisitor): void;
}

export class Employee implements Element {
    name: string;
    annualSalary: number;
    paidTimeOffDays: number;

    constructor(name: string, salary: number, timeOff: number) {
        this.name = name;
        this.annualSalary = salary;
        this.paidTimeOffDays = timeOff;
    }

    accept(visitor) {
        visitor.visit(this);
    }

}
