/**
 * class that contains the template method
 */
export abstract class Bread {
  abstract mixIngredients(): void;
  abstract bake(): void;

  slice() {
    console.log(`Slicing the bread!`)
  }

  /**
   * this is the template method
   */
  make() {
    this.mixIngredients();
    this.bake();
    this.slice();
  }
}
