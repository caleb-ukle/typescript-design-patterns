import {RecipeFactory} from "./base/RecipeFactory";
import {GrilledCheese, IceCreamSundae} from "./base/ConcreteProducts";

export class KidCuisineFactory extends RecipeFactory {

  createSandwich() {
    return new GrilledCheese()
  }

  createDessert() {
    return new IceCreamSundae()
  }

}
