import {ColdPrep} from "./base/ColdPrep";
import {Bar} from "./base/Bar";
import {HotPrep} from "./base/HotPrep";
import {IPatronOrder, Patron} from "./Patron";
import {Order} from "./base/Order";

export class Server {
  private _coldPrep = new ColdPrep();
  private _bar = new Bar();
  private _hotPrep = new HotPrep();

  placeOrder({appId, drinkId, entreeId}: IPatronOrder) {
    const order = new Order()
    order.appetizer = this._coldPrep.prepDish(appId);
    order.entree = this._hotPrep.prepDish(entreeId);
    order.drink = this._bar.prepDish(drinkId);

    return order;
  }
}
