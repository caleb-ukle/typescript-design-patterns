import {IExample} from "../utils/IExample";
import {RecipeFactory} from "./base/RecipeFactory";
import {AdultCuisineFactory} from "./AdultCuisineFactory";
import {KidCuisineFactory} from "./KidCuisineFactory";

const readline = require('readline-sync');

export class AbstractFactoryExample implements IExample {
  constructor() {
    console.log('Init Abstract Factory')
  }

  main() {
    const input = readline.question('Who are you? (A)dult or (C)hild? ') as string;
    let factory: RecipeFactory;

    switch (input.toUpperCase()) {
      case 'A':
        factory = new AdultCuisineFactory();
        break;
      case 'C':
        factory = new KidCuisineFactory();
        break;
      default:
        throw Error('Only options are A or C');
    }

    const sandwich = factory.createSandwich();
    const dessert = factory.createDessert();

    console.log(`Sandwich: ${sandwich.name}`);
    console.log(`Dessert: ${dessert.name}`);
  }
}
