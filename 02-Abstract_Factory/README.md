> [You should 100% support Matthew, since he authored the book from which these examples are from](https://exceptionnotfound.net/new-subscriber-benefit-get-the-daily-design-pattern-ebook/)
# Abstract Factory 

## What's the point?
Abstract Factory is a layer ontop of the [Factory Design Pattern](../01-Factory). The point of the abstract factory is to be a factory of factories. allows you to group loosely related objects together in a sane way of generating those objects

## Details
This example is about recipes. At the end of the day we want to get a sandwich and dessert for a kid vs an adult. To do this we create a recipe factory. We make two concrete factories, kid and adult. Each factory has a method to create a sandwich and dessert which is the sub factories, sandwich and dessert.
