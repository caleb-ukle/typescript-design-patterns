import {FoodItem} from "./FoodItem";

export interface IKitchenSection {
  prepDish(dishId: number): FoodItem
}
