export class Patron {
  constructor(name: string) {
    this._name = name;
  }

  private readonly _name: string;

  get name() {
    return this._name;
  }
}

export interface IPatronOrder {
  person: Patron,
  drinkId: number,
  appId: number,
  entreeId: number;
}
