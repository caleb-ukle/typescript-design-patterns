> [You should 100% support Matthew, since he authored the book from which these examples are from](https://exceptionnotfound.net/new-subscriber-benefit-get-the-daily-design-pattern-ebook/)
# Factory 

## What is the point?
It's all in the name, what does a factory do? it makes things.
The point of a factory is to define method and properties on a set of common objects.
Each object that is produces from the factory can define the specifics of what it wants inside those methods. 

## Details
This example is all about creating sandwich, so we make a sandwich factory and has a default `createIngredients` method. It's the responsibility of the class using the sandwich class to override and use the specific ingredients for that specific type of sandwich.  
