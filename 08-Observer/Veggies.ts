import {IRestaurant} from "./IRestaurant";

export abstract class Veggies {
    private _pricePerPound: number;
    private _restaurants: IRestaurant[] = [];


    protected constructor(pricePerPound: number) {
        this._pricePerPound = pricePerPound;
    }

    attach(restaurant: IRestaurant) {
        this._restaurants.push(restaurant);
    }

    detach(restaurant: IRestaurant) {
        const idx = this._restaurants.findIndex(r => r === restaurant);

        this._restaurants.splice(idx, 1);
    }

    notify() {
        this._restaurants.forEach(r => r.update(this))
    }

    get pricePerPound() {
        return this._pricePerPound
    }

    set pricePerPound(value) {
        if(this._pricePerPound != value) {
            this._pricePerPound = value;
            this.notify();
        }
    }
}
