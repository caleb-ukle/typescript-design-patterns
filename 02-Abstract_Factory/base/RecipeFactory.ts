import {Dessert, Sandwich} from "./AbstractProducts";

export abstract class RecipeFactory {
  abstract createSandwich(): Sandwich
  abstract createDessert(): Dessert
}
