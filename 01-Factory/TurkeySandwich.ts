import {Sandwich} from "./base/Sandwich";
import {Bread} from "./base/Bread";
import {Mayo} from "./base/Mayo";
import {Lettuce} from "./base/Lettuce";
import {Turkey} from "./base/Turkey";

export class TurkeySandwich extends Sandwich {

  createIngredients() {
    const ing = [
      new Bread(),
      new Mayo(),
      new Lettuce(),
      new Turkey,
      new Turkey(),
      new Bread()
    ];
    this.ingredients.push(...ing)
  }
}
