import {IKitchenSection} from "./IKitchenSection";
import {FoodItem} from "./FoodItem";

export class ColdPrep implements IKitchenSection {
  prepDish(dishId: number): FoodItem {
    return new FoodItem(dishId)
  }

}
