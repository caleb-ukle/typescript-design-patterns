import {IOrderingSystem} from "./OrderingSystem.interface";


/**
 * Concrete implementer for an ordering system at a diner
 */
export class DinerOrders implements IOrderingSystem {
  place(order: string): void {
    console.log(`Placing order for ${order} at the Diner`)
  }
}

/**
 * concrete implementer for an ordering system at fancy restaurant
 */
export class FancyRestaurantOrders implements IOrderingSystem {
  place(order: string): void {
    console.log(`Placing order for ${order} at the fancy restaurant`)
  }

}
