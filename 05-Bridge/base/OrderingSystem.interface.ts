/**
 * Implementer which defines an interface for placing an order
 */
export interface IOrderingSystem {
  place(order: string): void
}
