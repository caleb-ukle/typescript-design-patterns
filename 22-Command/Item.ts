export class MenuItem {

    constructor(
        public name: string,
        public amount: number,
        public price: number,
    ) {

    }

    display() {
        console.log(this);
    }
}
