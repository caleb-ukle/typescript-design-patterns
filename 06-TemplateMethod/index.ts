import {IExample} from "../utils/IExample";
import {Sourdough, TwelveGrain, WholeWheat} from "./base/concrete";

export class TemplateMethodExample implements IExample {
  main(): void {
    const sourdough = new Sourdough();
    sourdough.make();

    const grains = new TwelveGrain()
    grains.make();

    const wheat = new WholeWheat();
    wheat.make();
  }
}
