import {MenuItem} from "./Item";
import {OrderCommand} from "./command";

export class FastFoodOrder {
    currentItems: MenuItem[] = []

    constructor() {
    }

    executeCommand(command: OrderCommand, item: MenuItem) {
        command.execute(this.currentItems, item);
    }

    showCurrentItems() {
        console.log(this.currentItems);
    }
}
