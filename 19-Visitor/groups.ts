import {Employee} from "./immovable";
import {IVisitor} from "./visitors";

export class Employees {
    private _employees: Employee[] = [];

    attach(emp: Employee) {
        this._employees.push(emp);
    }

    detach(emp: Employee) {
        const idx = this._employees.findIndex(e => e.name === emp.name);
        this._employees.splice(idx, 1);
    }

    accept(visitor: IVisitor) {
        this._employees.forEach(e => e.accept(visitor));
    }
}

export class LineCook extends Employee {
    constructor() {
        super("Dmitri", 32000, 7);
    }
}

export class  HeadChef extends Employee {
    constructor() {
        super("Jackson", 69015, 21)
    }
}

export class GeneralManager extends Employee {
    constructor() {
        super("Amanda", 78000, 24)
    }
}
