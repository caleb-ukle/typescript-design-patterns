import {CommandExample} from "./22-Command";


// new FactoryExample().main();
// new AbstractFactoryExample().main();
// new FacadeExample().main()
// new AdaptorExample().main()
// new BridgeExample().main();
//
// new ObserverExample().main()
//     .catch(e => console.error(e))


// new VisitorExample().main();

// new CompositeExample().main();

// new MediatorExample().main()

new CommandExample().main();
