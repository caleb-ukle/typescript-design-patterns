import {SoftDrink} from "./SoftDrink";

export class OriginalCola extends SoftDrink {
    constructor(calories: number) {
        super(calories);
    }
}

export class CherryCola extends SoftDrink {
    constructor(calories: number) {
        super(calories);
    }
}

export class OGRootBeer extends SoftDrink {
    constructor(calories: number) {
        super(calories);
    }
}

export class VanillaRootBeer extends SoftDrink {
    constructor(calories: number) {
        super(calories);
    }
}

export class LemonLime extends SoftDrink {
    constructor(calories: number) {
        super(calories);
    }
}

