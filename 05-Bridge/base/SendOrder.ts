import {IOrderingSystem} from "./OrderingSystem.interface";

/**
 * Abstraction which represents the sent order and maintains
 * a ref to the restaurant where the order is going
 */
export abstract class SendOrder {
  /**
   * this is a ref to the implementer
   */
  _restaurant: IOrderingSystem;

  abstract send(): void
}
