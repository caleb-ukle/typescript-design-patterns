import {IExample} from "../utils/IExample";
import {Carrots} from "./Carrots";
import {Restaurant} from "./Restaurant";

export class ObserverExample implements IExample {
    async main() {
        const carrots = new Carrots(.82);
        carrots.attach(new Restaurant({name: "Mackay's", threshold: 77}));
        carrots.attach(new Restaurant({name: "Johnny's Sports Bar", threshold: 74}));
        carrots.attach(new Restaurant({name: "Salad Kingdom", threshold: 75}))

        let flag = true;

        while (flag) {
            setTimeout(() => flag = false, 30 * 1000)
            carrots.pricePerPound = Math.ceil(Math.random() * 100);
            await this.delay(Math.random() * 10000)
        }

    }

    delay(ms: number): Promise<void> {
        return new Promise((res) => {
            setTimeout(() => {
                res()
            }, ms)
        })
    }
}
