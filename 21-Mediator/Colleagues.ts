import {ConcessionStand, Mediator} from "./Mediator";

export class NorthConcessionStand extends ConcessionStand {
    constructor(mediator: Mediator) {
        super(mediator);
    }

    send(msg: string) {
        console.log('North:')
        super.send(msg);
    }

    notify(msg: string) {
        console.log('North:')
        super.notify(msg);
    }
}

export class SouthConcessionStand extends ConcessionStand {
    constructor(mediator: Mediator) {
        super(mediator);
    }
    send(msg: string) {
        console.log('South:')
        super.send(msg);
    }
    notify(msg: string) {
        console.log('South:')
        super.notify(msg);
    }
}
