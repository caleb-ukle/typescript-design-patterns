import {FoodItem} from "./FoodItem";

export class Order {
  appetizer: FoodItem;
  entree: FoodItem;
  drink: FoodItem;

  constructor() {
  }
}
