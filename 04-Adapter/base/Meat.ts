import {MeatDatabase, TemperatureType} from "./AncientAPI";

export class Meat {
  protected name: string;
  protected safeCookTempFahrenheit: number;
  protected safeCookTempCelsius: number;
  protected caloriesPerOunce: number;
  protected proteinPerOunce: number;

  constructor(meat: string) {
    this.name = meat;
  }

  /**
   * Methods are all virtual by default
   */
  loadData() {
    console.log(`\tMeat: ${this.name}`)
  }

}


export class MeatDetails extends Meat {
  private _db: MeatDatabase;

  constructor(name: string) {
    super(name);
  }

  loadData() {
    this._db = new MeatDatabase();
    this.safeCookTempFahrenheit = this._db.getSafeCookTemp(this.name, TemperatureType.Fahrenheit);
    this.safeCookTempCelsius = this._db.getSafeCookTemp(this.name, TemperatureType.Celsius);
    this.caloriesPerOunce = this._db.getCaloriesPerOunce(this.name);
    this.proteinPerOunce = this._db.getProteinPerOunce(this.name);

    super.loadData();
    console.log(`\tSafe Temp (F): ${this.safeCookTempFahrenheit}`)
    console.log(`\tSafe Temp (C): ${this.safeCookTempCelsius}`)
    console.log(`\tCalories per Ounce: ${this.caloriesPerOunce}`)
    console.log(`\tProtein per Ounce: ${this.proteinPerOunce}`)

  }

}
