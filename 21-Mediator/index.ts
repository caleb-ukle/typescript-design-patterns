import {IExample} from "../utils/IExample";
import {Intercom} from "./concrete";
import {NorthConcessionStand, SouthConcessionStand} from "./Colleagues";

export class MediatorExample implements IExample {
    main() {
        const mediator = new Intercom();
        // what if I turn around??
        const left = new NorthConcessionStand(mediator);
        const right = new SouthConcessionStand(mediator);

        mediator.add(left);
        mediator.add(right);

        mediator.North = left;
        mediator.South = right;

        left.send('🍿🍿🍿🍿 ???')
        right.send('👍 🏃');

        right.send("🌭?")
        left.send("🙅‍")
    }
}
