import {Server} from "./Server";
import {Patron} from "./Patron";
import {IExample} from "../utils/IExample";

const readlineSync = require('readline-sync');

export class FacadeExample implements IExample {
  constructor() {
    console.log('Init Facade Example')
  }

  main() {
    const server = new Server();
    const name = readlineSync.question("Hello! I'll be your server today. What is your name? ")

    const patron = new Patron(name);
    console.log(`Welcome ${patron.name}`);

    const drinkOptions = ["Water", "Soda", "Whisky Sour"];
    const appOptions = ["Salad"];
    const entreeOptions = ["Steak", "Chicken"];

    const drinkId = readlineSync.keyInSelect(drinkOptions, "Which drink would you like? ")

    const appId = readlineSync.keyInSelect(appOptions, "Which appetizer would you like? ")

    const entreeId = readlineSync.keyInSelect(entreeOptions, "Which entree would you like? ")

    console.log('Coming Right up')
    const order = server.placeOrder({
      person: patron,
      appId,
      drinkId,
      entreeId,
    })

    setTimeout(() => {
      console.log(`Order placed, and here is your drink, ${drinkOptions[order.drink.dishId]}`)
    }, 1000)

    setTimeout(() => {
      console.log(`Here is your appetizer, ${appOptions[order.appetizer.dishId]}`)
    }, 5000)

    setTimeout(() => {
      console.log(`Finally, here is your entree, ${entreeOptions[order.entree.dishId]}`)
    }, 10000)


  }

}
