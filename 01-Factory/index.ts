import {TurkeySandwich} from "./TurkeySandwich";
import {Dagwood} from "./Dagwood";
import {IExample} from "../utils/IExample";

export class FactoryExample implements IExample {
  constructor() {
    console.log('Init Factory Example')
  }
  main() {
    const turkey = new TurkeySandwich();
    const dagwood = new Dagwood();

    console.log(`Turkey sandwich has ${turkey.ingredients.length} ingredients`)

    console.log(`Dagwood sandwich has ${dagwood.ingredients.length} ingredients`)
    // now you'd used these sandwiches like calling a .eat() method etc.
  }
}
