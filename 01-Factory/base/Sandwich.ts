import {Ingredient} from "./Ingredient";

export abstract class Sandwich {
  constructor() {
    this.createIngredients()
  }

  private _ingredients: Ingredient[] = [];

  get ingredients() {
    return this._ingredients;
  }

  /**
   * this is the factory method
   */
  abstract createIngredients()

}
