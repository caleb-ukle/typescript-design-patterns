import {IExample} from "../utils/IExample";
import {Employees, GeneralManager, HeadChef, LineCook} from "./groups";
import {IncomeVisitor, IVisitor, PaidTimeOffVisitor} from "./visitors";

export class VisitorExample implements IExample {
    main() {
        const emps = new Employees()

        emps.attach(new LineCook())
        emps.attach(new HeadChef())
        emps.attach(new GeneralManager())

        emps.accept(new PaidTimeOffVisitor())
        emps.accept(new IncomeVisitor());
    }
}
