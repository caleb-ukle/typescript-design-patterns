import {IExample} from "../utils/IExample";
import {SodaDispenser} from "./Components";
import {CherryCola, OGRootBeer, OriginalCola, VanillaRootBeer} from "./Leaves";

export class CompositeExample implements IExample {
    main() {
        const fountain = new SodaDispenser();
        fountain.rootBeers.availableFlavors.push(new OGRootBeer(225))
        fountain.rootBeers.availableFlavors.push(new VanillaRootBeer(225))
        fountain.lemonLime.calories = 180;
        fountain.colas.availableFlavors.push(new OriginalCola(220));
        fountain.colas.availableFlavors.push(new CherryCola(230));

        fountain.displayCalories();
    }
}
