/**
 * Invoker class
 */
import {MenuItem} from "./Item";
import {AddCommand, ModifyCommand, OrderCommand, RemoveCommand} from "./command";
import {FastFoodOrder} from "./reciever";

export class Patron  {
    private _orderCommand: OrderCommand;
    private _menuItem: MenuItem;
    private _order: FastFoodOrder;

    constructor() {
        this._order = new FastFoodOrder();
    }

    setCommand(option: CommandOption) {
        this._orderCommand = new CommandFactory().getCommand(option);
    }

    setMenuItem(item: MenuItem) {
        this._menuItem = item;
    }

    executeCommand() {
        this._order.executeCommand(this._orderCommand, this._menuItem)
    }

    showCurrentOrder() {
        this._order.showCurrentItems();
    }
}

export class CommandFactory {
    getCommand(option: CommandOption) {
        switch (option) {
            case CommandOption.Add:
                return new AddCommand();

            case CommandOption.Modify:
                return new ModifyCommand();

            case CommandOption.Remove:
                return new RemoveCommand();

            default:
                return new AddCommand();
        }
    }
}

export enum CommandOption {
    Add,
    Modify,
    Remove

}
