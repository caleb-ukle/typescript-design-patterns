export enum TemperatureType {
  Fahrenheit,
  Celsius
}

/**
 * This represents the legacy api we want to convert
 */
export class MeatDatabase {
  getSafeCookTemp(meat: string, tempType: TemperatureType): number {
    if(tempType === TemperatureType.Fahrenheit) {
      switch (meat.toLowerCase()) {
        case "beef":
        case "veal":
        case "pork":
          return 145;
        case "chicken":
        case "turkey":
          return 165;
        default:
          return 165;
      }
    } else {
      switch (meat.toLowerCase()) {
        case "beef":
        case "veal":
        case "pork":
          return 63;
        case "chicken":
        case "turkey":
          return 74;
        default:
          return 74;
      }
    }
  }

  getCaloriesPerOunce(meat: string): number {
    switch (meat.toLowerCase()) {
      case "beef":
        return 71;
      case "pork":
        return 69;
      case "chicken":
        return 66;
      case "turkey":
        return 38;
      default:
        return 0;
    }
  }

  getProteinPerOunce(meat: string): number {
    switch (meat.toLowerCase()) {
      case "beef":
        return 7.33;
      case "pork":
        return 7.67;
      case "chicken":
        return 8.57;
      case "turkey":
        return 8.5;
      default:
        return 0;
    }
  }
}
