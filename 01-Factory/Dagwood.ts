import {Sandwich} from "./base/Sandwich";
import {Bread} from "./base/Bread";
import {Turkey} from "./base/Turkey";
import {Lettuce} from "./base/Lettuce";
import {Mayo} from "./base/Mayo";

/**
 * A Dagwood is a ridiculously large
 * sandwich, with many layers of bread and fillings.
 */
export class Dagwood extends Sandwich {
  createIngredients() {
    const ing = [
      new Bread(),
      new Turkey(),
      new Turkey(),
      new Lettuce(),
      new Lettuce(),
      new Mayo(),
      new Bread(),
      new Turkey(),
      new Turkey(),
      new Lettuce(),
      new Lettuce(),
      new Mayo(),
      new Bread(),
      new Turkey(),
      new Turkey(),
      new Lettuce(),
      new Lettuce(),
      new Mayo(),
      new Bread(),
      new Turkey(),
      new Turkey(),
      new Lettuce(),
      new Lettuce(),
      new Mayo(),
      new Bread(),
      new Turkey(),
      new Turkey(),
      new Lettuce(),
      new Lettuce(),
      new Mayo(),
      new Bread(),
      new Turkey(),
      new Turkey(),
      new Lettuce(),
      new Lettuce(),
      new Mayo(),
      new Bread(),
    ];

    this.ingredients.push(...ing);

  }
}
