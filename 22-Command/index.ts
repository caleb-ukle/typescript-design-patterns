import {IExample} from "../utils/IExample";
import {CommandOption, Patron} from "./patron";
import {MenuItem} from "./Item";

export class CommandExample implements IExample {
    main() {
        const p = new Patron();

        p.setCommand(CommandOption.Add)
        p.setMenuItem(new MenuItem("Fries", 2, 1.99));
        p.executeCommand()

        p.setCommand(CommandOption.Add) // why do I need to keep setting the command?
        p.setMenuItem(new MenuItem("Burger", 2, 2.59));
        p.executeCommand()

        p.setMenuItem(new MenuItem("Drink", 2, 1.99))
        p.executeCommand()

        p.showCurrentOrder()

        p.setCommand(CommandOption.Remove)
        p.setMenuItem(new MenuItem("Fries", 2., 1.99))
        p.executeCommand()

        p.setCommand(CommandOption.Modify)
        p.setMenuItem(new MenuItem("Burger", 4, 8))
        p.executeCommand()

        p.showCurrentOrder()


    }
}
