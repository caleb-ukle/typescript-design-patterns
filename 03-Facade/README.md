> [You should 100% support Matthew, since he authored the book from which these examples are from](https://exceptionnotfound.net/new-subscriber-benefit-get-the-daily-design-pattern-ebook/)
# Facade

## What's the point?
This pattern is also in the name, Facade. A facade is the "front" of something. Meaning we want the facade to be the front of some more complex code.

## Details
This example takes the concept of a restaurant. A restaurant has multiple places and ways of preparing food based on what is orders. At the end of the day the individual visiting the restaurant, _patron_, doesn't care about how the kitchen operates, just that they can get food. So that's where the Server comes in, the Sever is the facade that is the front/presenting layer to the patron. the server is the interfacing layer between our more complex logic, _the kitchen_, and the patron. 
