import {Veggies} from "./Veggies";

export interface IRestaurant {
    update(veggies: Veggies): void
}
