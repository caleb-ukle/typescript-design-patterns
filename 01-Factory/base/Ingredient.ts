/**
 * TypeScript has abstract but in JS there isn't abstract classes,
 * the point is you cannot "new up" an abstract class,
 * so just know the abstract class is for inheritance
 */
export abstract class Ingredient {

}
