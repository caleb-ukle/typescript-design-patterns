import {Dessert, Sandwich} from "./AbstractProducts";

export class BLT extends Sandwich {
  readonly name = 'BLT'
}

export class CremeBrulee extends Dessert {
  readonly name = 'Creme Brulee'
}

export class GrilledCheese extends Sandwich {
  readonly name = 'Grilled Cheese'
}

export class IceCreamSundae extends Dessert {
  readonly name = 'Ice Cream Sundae'
}
