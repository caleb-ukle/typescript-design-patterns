import {Bread} from "./bread";

export class TwelveGrain extends Bread {
  bake(): void {
    console.log(`baking the 12-grain bread`)
  }

  mixIngredients(): void {
    console.log(`Mixing ingredients for 12-grain bread`)
  }

}

export class Sourdough extends Bread {
  mixIngredients(): void {
    console.log(`Mixing ingredients for sourdough bread`)
  }
  bake(): void {
    console.log(`baking the sourdough bread`)
  }
}

export class WholeWheat extends Bread {
  mixIngredients(): void {
    console.log(`Mixing ingredients for whole wheat bread`)
  }

  bake(): void {
    console.log(`baking the whole wheat bread`)
  }
}
