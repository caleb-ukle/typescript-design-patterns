import {Element, Employee} from "./immovable";

export interface IVisitor {
    visit(el: Element): void;
}

export class IncomeVisitor implements IVisitor {
    visit(el: Element) {
        const emp = el as Employee;
        console.log('before', emp)
        // add 10% raise
        emp.annualSalary *= 1.10;
        console.log('after', emp);
    }
}

export class PaidTimeOffVisitor implements IVisitor {
    visit(el: Element) {
        const emp = el as Employee;
        console.log('before', emp)

        emp.paidTimeOffDays += 3;
        console.log('after', emp);
    }
}
