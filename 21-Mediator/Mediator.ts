export interface Mediator {
    sendMessage(message: string, concessionStand: ConcessionStand): void;
}

export interface IChatable {
    send(msg: string):void;
    notify(msg: string): void;
}

export abstract class ConcessionStand implements IChatable {
    protected mediator: Mediator;

    constructor(
        mediator: Mediator
    ) {
        this.mediator = mediator;
    }

    send(msg: string): void {
        console.log(`Sending Message ${msg}`)
        this.mediator.sendMessage(msg, this);
    }

    notify(msg: string): void {
        console.log(`New Message ${msg}`)
    }

}
