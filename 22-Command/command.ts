import {MenuItem} from "./Item";

export abstract class OrderCommand {
    abstract execute(order: MenuItem[], newItem: MenuItem): void;
}

export class AddCommand extends OrderCommand {
    constructor() {
        super();
    }

    execute(order: MenuItem[], newItem: MenuItem): void {
        order.push(newItem);
    }
}

export class RemoveCommand extends OrderCommand {
    execute(order: MenuItem[], newItem: MenuItem): void {
        const itemIdx = order.findIndex(o => o.name === newItem.name)

        if (itemIdx === -1) {
            throw new Error("It ain't here fam")
        }

        order.splice(itemIdx, 1);
    }
}

export class ModifyCommand extends OrderCommand {
    execute(order: MenuItem[], newItem: MenuItem): void {
        const item = order.find(o => o.name === newItem.name);
        if (!item) {
            throw new Error("It ain't here fam")
        }
        item.amount = newItem.amount;
        item.price = newItem.price;
    }

}
