import {RecipeFactory} from "./base/RecipeFactory";
import {BLT, CremeBrulee} from "./base/ConcreteProducts";

export class AdultCuisineFactory extends RecipeFactory {
  createSandwich() {
    return new BLT()
  }
  createDessert() {
    return new CremeBrulee();
  }
}
