import {SendOrder} from "./SendOrder";

/**
 * A refined abstraction for a dairy-free order
 */
export class SendDairyFreeOrder extends SendOrder {
  send(): void {
    this._restaurant.place('Dairy-Free Order')
  }

}

/**
 * A refined abstraction for gluten-free order
 */
export class SendGlutenFreeOrder extends SendOrder {
  send(): void {
    this._restaurant.place('Gluten-Free Order')
  }
}
