import {IExample} from "../utils/IExample";
import {SendDairyFreeOrder, SendGlutenFreeOrder} from "./base/RefinedAbstractions";
import {DinerOrders, FancyRestaurantOrders} from "./base/ConcreteImplementer";
import {SendOrder} from "./base/SendOrder";

export class BridgeExample implements IExample {
  main(): void {
    let order: SendOrder = new SendDairyFreeOrder();
    order._restaurant = new DinerOrders();
    order.send();

    order._restaurant = new FancyRestaurantOrders();
    order.send();

    order = new SendGlutenFreeOrder();
    order._restaurant = new DinerOrders();
    order.send();

    order._restaurant = new FancyRestaurantOrders()
    order.send();
  }
}
