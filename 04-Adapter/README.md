> [You should 100% support Matthew, since he authored the book from which these examples are from](https://exceptionnotfound.net/new-subscriber-benefit-get-the-daily-design-pattern-ebook/)
# Adaptor

## What's the point?
This pattern is used for when you map/tweak an existing subsystem to a new one. Matthew makes a point that Adaptor and Facade are similar but for different purposes. Adaptor is for more of one-to-one mapping to subsystems. While the goal of a Facade is to hide complexity of potentially several subsystems and providing an interface to work with.

## Details
This example takes a mocked legacy database for knowing safe cooking temps and protein/calorie contents of meat and adapts it into a more OOP style. With the adapted style you just create a `new MeatDetails(_meatName_)` and then call `.loadData()` method to pull that info from the _legacy api_ and added to class fields. Without the adaptor you would manually have to call each method from the database and compose your own object. You can think of this as a wrapper for the database. But the `MeatDetails` class is just a single mapping to the `MeatDatabase` class, where `MeatDetails` makes all the individual calls to the _legacy api_ like you would on the _legacy api_
