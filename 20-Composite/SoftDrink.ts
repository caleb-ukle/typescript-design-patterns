export abstract class SoftDrink {
    calories: number;

    protected constructor(calories: number) {
        this.calories = calories;
    }
}
