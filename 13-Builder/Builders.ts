export abstract class SandwichBuilder {
  protected _sandwich: Sandwich

  get sandwich() {
    return this._sandwich;
  }

  abstract AddBread(): void;

  abstract AddMeats(): void;

  abstract AddCheese(): void;

  abstract AddVeggies(): void;

  abstract AddCondiments(): void;
}

export class TurkeyClub extends SandwichBuilder {

  constructor() {
    super();
    this._sandwich = new Sandwich('Turkey Club')
  }

  AddBread(): void {
  this._sandwich['bread'] = '12-grain';
  }

  AddMeats(): void {
    this._sandwich['meat'] = 'Turkey'
  }

  AddCheese(): void {
    this._sandwich['cheese'] = 'Swiss'
  }

  AddVeggies(): void {
    this._sandwich['veggies'] = 'Lettuce, Tomato'
  }

  AddCondiments(): void {
    this._sandwich['condiments'] = 'Mayo';
  }
}


class Sandwich {
  [ingredient: string]: string;

  private readonly _type: string;

  get type() {
    return this._type;
  }

  constructor(name) {
    this._type = name
  }

  // show() {
  //
  // }
}

