import {IRestaurant} from "./IRestaurant";
import {Veggies} from "./Veggies";

export class Restaurant implements IRestaurant {
    private _name: string;
    private _veggie: Veggies;
    private _purchaseThreshold: number;

    constructor({name, threshold}) {
        this._name = name;
        this._purchaseThreshold = threshold
    }

    update(veggies: Veggies): void {
        console.log(`Notified ${this._name} of ${this._veggie?.constructor?.name} price changed to ${this._purchaseThreshold}`)


        if(veggies.pricePerPound < this._purchaseThreshold) {
            console.log(`${this._name} wants to buy some ${this._veggie?.constructor?.name}`)
        }
    }

}
